/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pprog_tp3.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.Calendar;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Scanner;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pprog_tp3.model.AcessoAreaRestrita;
import pprog_tp3.model.AreaRestrita;
import pprog_tp3.model.AtribuirCartaoDefinitivo;
import pprog_tp3.model.AtribuirCartaoProvisorio;
import pprog_tp3.model.Cartao;
import pprog_tp3.model.CartaoIdentificacao;
import pprog_tp3.model.Colaborador;
import pprog_tp3.model.Empresa;
import pprog_tp3.model.Equipamento;
import pprog_tp3.model.FicheiroRegistoAcessos;
import pprog_tp3.model.PerfilDeAutorizacao;
import pprog_tp3.model.RegistoDeAcessos;
import pprog_tp3.model.RegistoDeCartoes;
import pprog_tp3.model.RegistoDeEquipamentos;
import pprog_tp3.ui.FileChooserRegistoPerfis;

/**
 * FXML Controller class
 *
 * @author André e Carlos
 */
public class JanelaPrincipalController implements Initializable {

    @FXML
    private TextField numCartao;
    @FXML
    private TextField txtIdEquipamento;
    @FXML
    private Button btnAceder;
    @FXML
    private Button btnSair;

    private Stage registarPerfilStage;

    private Stage definirLotacaoMaximaStage;

    private List<Equipamento> e;

    @FXML
    private TextArea txtEquipamentos;
    @FXML
    private TextField txtData;
    @FXML
    private TextField txtHora;

    private RegistoDeEquipamentos re;
    private RegistoDeCartoes rc;
    
    private List<AtribuirCartaoDefinitivo> aCd;
    private List<AtribuirCartaoProvisorio> aCp;
    
    RegistoDeAcessos a = new RegistoDeAcessos();

    Equipamento eq = new Equipamento();
    CartaoIdentificacao cart = new CartaoIdentificacao();
    Colaborador colab = new Colaborador();
    PerfilDeAutorizacao p = new PerfilDeAutorizacao();
    AreaRestrita area = new AreaRestrita();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            registarPerfilStage = new Stage();
            registarPerfilStage.initModality(Modality.APPLICATION_MODAL);

            registarPerfilStage.setTitle("Registar Perfil");
            registarPerfilStage.setResizable(false);

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/RegistarPerfilScene.fxml"));
            Parent root = loader.load();

            Scene scene = new Scene(root);

            RegistarPerfilController ncController = loader.getController();
            ncController.associarController(this);

            registarPerfilStage.setScene(scene);

        } catch (IOException ex) {
            criarAlerta(Alert.AlertType.ERROR, "Erro", ex.getMessage());
        }
    }

    /**
     * Método que associa o equipamento à lista de equipamentos.
     *
     * @param eq a lista de equipamentos
     */
    public void associarEquipamentos(List<Equipamento> eq) {
        this.e = eq;
        atualizaTextBoxRegistoEquip();
    }

    /**
     * Método que atualiza a textbox do registo dos equipamentos.
     */
    private void atualizaTextBoxRegistoEquip() {
        String equip = "";
        for (Equipamento equipamento : e) {
            String listaEquipamentosString = equipamento.toString();
            equip = equip + listaEquipamentosString;
        }
        txtEquipamentos.setText(equip);
    }

    @FXML
    private void acederAreaRestrita(ActionEvent event) throws EquipamentoException, FileNotFoundException {
        try {
            List<AtribuirCartaoDefinitivo> lstAcd = lerFichAtribuicaoDefinitiva();
            List<AtribuirCartaoProvisorio> lstAcp = lerFichAtribuicaoProvisoria();
            String nCartao = numCartao.getText().trim();
            String idEquip = txtIdEquipamento.getText().trim();
            int cont = 0;
            Equipamento equip = null;
            for (Equipamento equipamento : e) {
                cont++;
                if (idEquip.equals(equipamento.getEnderecoLogico())) {
                    equip = new Equipamento(equipamento.getEnderecoLogico(), equipamento.getEnderecoFisico(),
                            equipamento.getDescricao(), equipamento.getFichConfig(), equipamento.getMovimento(), equipamento.getFabricante());
                    break;
                } else {
                    if (cont == e.size()) {
                        throw new EquipamentoException();
                    }
                }
            }
            for (AtribuirCartaoDefinitivo acd : lstAcd) {
                String[] cartao = acd.getCartao().split(";");
                String[] colab = acd.getM_sColaborador().split(";");
                String[] perfil = colab[3].split("_");
                String[] equips = perfil[5].split("&");
                if (equips[0].equals(idEquip)) {
                    if (nCartao.equals(cartao[0])) {
                        String[] horaI = perfil[3].split(":");
                        String[] horaF = perfil[4].split(":");
                        horaI[0] = Integer.toString(Integer.parseInt(horaI[0]) - 12);
                        horaF[0] = Integer.toString(Integer.parseInt(horaF[0]) - 12);
                        Calendar tempoAgora = Calendar.getInstance();
                        String hora = "0" + Integer.toString(tempoAgora.get(Calendar.HOUR));
                        String minuto = Integer.toString(tempoAgora.get(Calendar.MINUTE));
                        int dSemana = tempoAgora.get(Calendar.DAY_OF_WEEK);
                        String diaSemana = encontrarDiaSemana(dSemana);
                        if (Integer.parseInt(hora) >= Integer.parseInt(horaI[0]) && Integer.parseInt(hora) <= Integer.parseInt(horaF[0])
                                && Integer.parseInt(minuto) >= Integer.parseInt(horaI[1]) && Integer.parseInt(minuto) <= Integer.parseInt(horaF[1])
                                && diaSemana.equals(perfil[2])) {
                            AcessoAreaRestrita aar = new AcessoAreaRestrita(equip,acd.getCartao(),acd.getM_sColaborador(),Integer.toString(Calendar.DATE), hora+":"+minuto,equips[4],"sucesso","motivo");
                            a.addAcesso(aar);
                            exportarFich();
                            criarAlerta(Alert.AlertType.INFORMATION, "Autorizacão Acesso", "Colaborador Autorizado").show();
                        } else {
                            criarAlerta(Alert.AlertType.INFORMATION, "Autorizacão Acesso", "Colaborador Não Autorizado").show();
                        }
                    }
                } else {
                    criarAlerta(Alert.AlertType.INFORMATION, "Autorizacão Acesso", "Colaborador Não Autorizado").show();
                }
            }
            for (AtribuirCartaoProvisorio acp : lstAcp) {
                String[] cartao = acp.getCartao().split(";");
                String[] colab = acp.getM_sColaborador().split(";");
                String[] perfil = colab[3].split("_");
                String[] equips = perfil[5].split("&");
                if (equips[0].equals(idEquip)) {
                    if (nCartao.equals(cartao[0])) {
                        String[] horaI = perfil[3].split(":");
                        String[] horaF = perfil[4].split(":");
                        horaI[0] = Integer.toString(Integer.parseInt(horaI[0]) - 12);
                        horaF[0] = Integer.toString(Integer.parseInt(horaF[0]) - 12);
                        Calendar tempoAgora = Calendar.getInstance();
                        String hora = "0" + Integer.toString(tempoAgora.get(Calendar.HOUR));
                        String minuto = Integer.toString(tempoAgora.get(Calendar.MINUTE));
                        int dSemana = tempoAgora.get(Calendar.DAY_OF_WEEK);
                        String diaSemana = encontrarDiaSemana(dSemana);
                        if (Integer.parseInt(hora) >= Integer.parseInt(horaI[0]) && Integer.parseInt(hora) <= Integer.parseInt(horaF[0])
                                && Integer.parseInt(minuto) >= Integer.parseInt(horaI[1]) && Integer.parseInt(minuto) <= Integer.parseInt(horaF[1])
                                && diaSemana.equals(perfil[2])) {
                            AcessoAreaRestrita aar = new AcessoAreaRestrita(equip,acp.getCartao(),acp.getM_sColaborador(),Integer.toString(Calendar.DATE), hora+":"+minuto,equips[4],"sucesso","motivo");
                            a.addAcesso(aar);
                            exportarFich();
                            criarAlerta(Alert.AlertType.INFORMATION, "Autorizacão Acesso", "Colaborador Autorizado").show();
                        } else {
                            criarAlerta(Alert.AlertType.INFORMATION, "Autorizacão Acesso", "Colaborador Não Autorizado").show();
                        }
                    }
                } else {
                    criarAlerta(Alert.AlertType.INFORMATION, "Autorizacão Acesso", "Colaborador Não Autorizado").show();
                }
            }
        } catch (EquipamentoException eqe) {
            criarAlerta(Alert.AlertType.ERROR, "Equipamento Inexistente", "Equipamento Inexistente").show();
        }
    }

    private String encontrarDiaSemana(int dSemana) {
        String a = null;
        if (dSemana == 1) {
            a = "Domingo";
        }
        if (dSemana == 2) {
            a = "Segunda-Feira";
        }
        if (dSemana == 3) {
            a = "Terca-Feira";
        }
        if (dSemana == 4) {
            a = "Quarta-Feira";
        }
        if (dSemana == 5) {
            a = "Quinta-Feira";
        }
        if (dSemana == 6) {
            a = "Sexta-Feira";
        }
        if (dSemana == 7) {
            a = "Sabado";
        }
        return a;
    }

    @FXML
    private void sair(ActionEvent event) {
        System.exit(0);
    }

    /**
     * Método para criar alerta.
     *
     * @param tipoAlerta o tipo de alerta
     * @param cabecalho o cabeçalho do alerta
     * @param mensagem a mensagem do alerta
     *
     * @return alerta
     */
    private Alert criarAlerta(Alert.AlertType tipoAlerta, String cabecalho,
            String mensagem) {
        Alert alerta = new Alert(tipoAlerta);

        alerta.setTitle("Aplicação");
        alerta.setHeaderText(cabecalho);
        alerta.setContentText(mensagem);

        return alerta;
    }

    @FXML
    private void registarNovoPerfilAction(ActionEvent event) throws IOException {
        registarPerfilStage.show();
    }

    @FXML
    private void importarEquipamentosOnAction(ActionEvent event) {
    }

    @FXML
    private void menuLotacaoMaximaAction(ActionEvent event) {
        definirLotacaoMaximaStage.show();
    }

    @FXML
    private void menuOpcoesAcercaAction(ActionEvent event) {
        criarAlerta(Alert.AlertType.INFORMATION, "Acerca",
                "@Copyright\nPPROG 2017/2018").show();
    }
    
    public List<AtribuirCartaoDefinitivo> lerFichAtribuicaoDefinitiva() throws FileNotFoundException {
        String[] atribuicaoD = new String[3];
        RegistoDeCartoes listaAtribuicaoDefinitiva = new RegistoDeCartoes();
        File file = new File("Atribuicoes.txt");
        Scanner scan = new Scanner(file);
        while (scan.hasNext() == true) {
            atribuicaoD = scan.nextLine().trim().split(" - ");
            String[] atribuicaoD2 = atribuicaoD[0].split(";");
            if (atribuicaoD2[3].equals("Definitivo")) {
                AtribuirCartaoDefinitivo ad = new AtribuirCartaoDefinitivo(atribuicaoD[0], atribuicaoD[1], atribuicaoD[2]);
                listaAtribuicaoDefinitiva.atribuirCartaoDefinitivo(ad);
            }
        }
        return listaAtribuicaoDefinitiva.getListaAtribuicaoCartoesDefinitivos();
    }

    public List<AtribuirCartaoProvisorio> lerFichAtribuicaoProvisoria() throws FileNotFoundException {
        String[] atribuicaoP = new String[4];
        RegistoDeCartoes listaAtribuicaoProvisoria = new RegistoDeCartoes();
        File file = new File("Atribuicoes.txt");
        Scanner scan = new Scanner(file);
        while (scan.hasNext() == true) {
            atribuicaoP = scan.nextLine().trim().split(" - ");
            String[] atribuicaoP2 = atribuicaoP[0].split(";");
            if (atribuicaoP2[3].equals("Provisorio")) {
                AtribuirCartaoProvisorio ap = new AtribuirCartaoProvisorio(atribuicaoP[0], atribuicaoP[1], atribuicaoP[2], atribuicaoP[3]);
                listaAtribuicaoProvisoria.atribuirCartaoProvisorio(ap);
            }
        }
        return listaAtribuicaoProvisoria.getListaAtribuicaoCartoesProvisorios();
    }
    
    private void exportarFich(){
        FileChooser flChooser = FileChooserRegistoPerfis.criarFileChooserRegistoPerfis();
        File ficheiroExportar = new File("acessos.ltf");
        FicheiroRegistoAcessos fra = new FicheiroRegistoAcessos();
        if (fra.guardar(ficheiroExportar, a)) {
                criarAlerta(Alert.AlertType.INFORMATION, "Exportar Lista",
                        "Contactos exportados com sucesso.").show();
            } else {
                criarAlerta(Alert.AlertType.ERROR, "Exportar Lista",
                        "Problema a exportar a lista!").show();
            }
    }
}
