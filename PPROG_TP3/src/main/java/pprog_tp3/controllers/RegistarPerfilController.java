/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pprog_tp3.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Scanner;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pprog_tp3.model.Empresa;
import pprog_tp3.model.Equipamento;
import pprog_tp3.model.ListaPeriodosAutorizacao;
import pprog_tp3.model.PerfilDeAutorizacao;
import pprog_tp3.model.PeriodoDeAutorizacao;
import pprog_tp3.model.RegistoDeEquipamentos;
import pprog_tp3.model.RegistoPerfisDeAutorizacao;

/**
 * FXML Controller class
 *
 * @author André e Carlos
 */
public class RegistarPerfilController implements Initializable {

    @FXML
    private Button btnSair;
    @FXML
    private Button btnSelecionarPeriodo;

    private JanelaPrincipalController jpController;

    private Stage selecionarPeriodoStage;

    private Stage registarPerfilStage;
    @FXML
    public TextField txtId;
    @FXML
    public TextField txtDescricao;

    private List<Equipamento> e;
    
    private RegistoDeEquipamentos re;
    
    
    private List<PerfilDeAutorizacao> lp;
    
    public List<PeriodoDeAutorizacao> lpa;
    
    
    private ArrayList<Equipamento> le;
    PerfilDeAutorizacao p = new PerfilDeAutorizacao();

    private ListaPeriodosAutorizacao lsp;
    
    SelecionarPeriodoController spc;
    private RegistoPerfisDeAutorizacao rp;
    

    /**
     * Initializes the controller class.
     * @param url url
     * @param rb rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            selecionarPeriodoStage = new Stage();
            selecionarPeriodoStage.initModality(Modality.APPLICATION_MODAL);

            selecionarPeriodoStage.setTitle("Selecionar Período");
            selecionarPeriodoStage.setResizable(false);

            List<Equipamento> lst = lerFichEquipamentos();

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/SelecionarPeriodoScene.fxml"));
            Parent root = loader.load();

            Scene scene = new Scene(root);

            SelecionarPeriodoController ncController = loader.getController();
            ncController.associarController1(this);

            SelecionarPeriodoController spController = loader.getController();
            spController.associarEquipamentos(lst);

            selecionarPeriodoStage.setScene(scene);

        } catch (IOException ex) {
            criarAleta(Alert.AlertType.ERROR, "Erro", ex.getMessage());
        }
    }

    @FXML
    private void sair(ActionEvent event) {
        encerrarRegistarPerfil(event);
    }

    /**
     * Método que permite associar os controllers JanelaPrincipal e 
     * RegistarPerfil.
     * 
     * @param jpController janelaprincipalController
     */
    public void associarController(JanelaPrincipalController jpController) {
        this.jpController = jpController;
    }

    @FXML
    private void selecionarPeriodoAction(ActionEvent event) throws IOException {
        selecionarPeriodoStage.show();
    }

    /**
     * Método para criar alerta.
     * 
     * @param tipoAlerta o tipo de alerta
     * @param cabecalho o cabeçalho do alerta
     * @param mensagem a mensagem do alerta
     * 
     * @return alerta
     */
    private Alert criarAleta(Alert.AlertType tipoAlerta, String cabecalho,
            String mensagem) {
        Alert alerta = new Alert(tipoAlerta);

        alerta.setTitle("Aplicação");
        alerta.setHeaderText(cabecalho);
        alerta.setContentText(mensagem);

        return alerta;
    }

    /**
     * Método que limpa as textboxes e esconde a janela.
     * 
     * @param event 
     */
    private void encerrarRegistarPerfil(ActionEvent event) {
        txtId.setText("");
        txtDescricao.setText("");

        ((Node) event.getSource()).getScene().getWindow().hide();
    }

    /**
     * Método que define os dados a inserir no perfil de autorização.
     * 
     * @param id o id do perfil
     * @param descricao a descrição do perfil
     * 
     * @return lista de equipamentos
     */
    public ArrayList<Equipamento> setDados(String id, String descricao) {
        p.setId(id);
        p.setDescricao(descricao);
        re = Empresa.getRegistoEquipamentos();
        le = (ArrayList<Equipamento>) re.getListaEquipamentos();
        return le;
    }

    /**
     * Adiciona um equipamento ao perfil de autorização.
     * 
     * @param e equipamento a adicionar
     */
    public void addEquipamento(Equipamento e) {
        p.addEquipamento(e);
    }

    /**
     * Retorna true se adicionar o período ao perfil de autorização e false caso
     * não adicione.
     * 
     * @param pa novo periodo de autorização
     * 
     * @return true caso adicione, false caso não adicione
     */
    public boolean addPeriodo(PeriodoDeAutorizacao pa) {
        if(p.addPeriodo(pa)){
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Retorna true se adicionar o perfil de autorização e false caso
     * não adicione.
     * 
     * @param p novo perfil de autorização
     * 
     * @return true caso adicione, false caso não adicione
     */
    boolean registarPerfil(PerfilDeAutorizacao p) {
        if (rp.adicionarPerfil(p)) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Método para ler o ficheiro dos equipamentos
     * 
     * @return lista de equipamentos
     * 
     * @throws FileNotFoundException a
     */
    public List<Equipamento> lerFichEquipamentos() throws FileNotFoundException {
        String[] equipamento = new String[6];
        RegistoDeEquipamentos listaEquipamentos = new RegistoDeEquipamentos();
        File file = new File("Equipamentos.txt");
        Scanner scan = new Scanner(file);
        while (scan.hasNext() == true) {
            equipamento = scan.nextLine().trim().split(";");
            Equipamento e1 = new Equipamento(equipamento[0], equipamento[1], equipamento[2], equipamento[3], equipamento[4], equipamento[5]);
            listaEquipamentos.registaEquipamento(e1);
        }
        return listaEquipamentos.getListaEquipamentos();
    }
    
    public String enviarId(){
        return txtId.getText().trim();
    }
    
    public String enviarDescricao(){
        return txtDescricao.getText().trim();
    }
}
