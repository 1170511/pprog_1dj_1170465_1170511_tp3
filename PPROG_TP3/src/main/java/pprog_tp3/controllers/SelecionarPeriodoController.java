package pprog_tp3.controllers;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pprog_tp3.model.Equipamento;
import pprog_tp3.model.FicheiroRegistoPerfis;
import pprog_tp3.model.ListaPeriodosAutorizacao;
import pprog_tp3.model.PerfilDeAutorizacao;
import pprog_tp3.model.PeriodoDeAutorizacao;
import pprog_tp3.model.RegistoPerfisDeAutorizacao;
import pprog_tp3.ui.FileChooserRegistoPerfis;

/**
 * FXML Controller class
 *
 * @author André e Carlos
 */
public class SelecionarPeriodoController extends RegistarPerfilController implements Initializable{
    
    ObservableList<String> DiasSemanaList = FXCollections.observableArrayList("Segunda-Feira", "Terça-Feira",
            "Quarta-Feira", "Quinta-feira", "Sexta-Feira", "Sábado", "Domingo");

    ObservableList<String> HorasList = FXCollections.observableArrayList("00", "01", "02", "03", "04", "05",
            "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23");

    ObservableList<String> MinutosList = FXCollections.observableArrayList("00", "01", "02", "03", "04", "05",
            "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26",
            "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47",
            "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59");

    @FXML
    private Button btnAdicionarPeriodo;
    @FXML
    private Button btnSair;

    private RegistarPerfilController rpController;

    @FXML
    private ChoiceBox cboxDiaSemana;
    @FXML
    private ChoiceBox cboxHoraInicio;
    @FXML
    private ChoiceBox cboxMinutoInicio;
    @FXML
    private ChoiceBox cboxHoraFim;
    @FXML
    private ChoiceBox cboxMinutoFim;

    private List<Equipamento> e;

    @FXML
    private TextField txtIdEquipamento;
    @FXML
    private TextArea txtMostrarEquipamentos;
    @FXML
    private Button btnAdicionarTotal;
    @FXML
    private Button btnAdicionarNula;

    public List<PeriodoDeAutorizacao> pa;

    ListaPeriodosAutorizacao lpa = new ListaPeriodosAutorizacao();
    
    RegistoPerfisDeAutorizacao rpa = new RegistoPerfisDeAutorizacao();
    @FXML
    private Button btnAdicionarPerfil;

    RegistarPerfilController rpControler;

    RegistoPerfisDeAutorizacao rp;
    @FXML
    private Label lblId;
    @FXML
    private Label lblDescricao;

    String a;
    String b;

    /**
     * Initializes the controller class.
     *
     * @param url url
     * @param rb rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cboxDiaSemana.setItems(DiasSemanaList);
        cboxHoraInicio.setItems(HorasList);
        cboxHoraFim.setItems(HorasList);
        cboxMinutoInicio.setItems(MinutosList);
        cboxMinutoFim.setItems(MinutosList);
    }

    /**
     * Método que permite associar os controllers SelecionarPeriodo e RegistarPerfil.
     * 
     * @param rpController registarperfilController
     */
    public void associarController1(RegistarPerfilController rpController) {
        this.rpController = rpController;
    }
    
    @FXML
    public void adicionarPeriodo(ActionEvent event) throws EquipamentoException, HoraException {
        try {
            String diaSemana = (String) cboxDiaSemana.getValue();
            String horaInicio = (String) (cboxHoraInicio.getValue() + ":" + cboxMinutoInicio.getValue());
            String horaFim = (String) (cboxHoraFim.getValue() + ":" + cboxMinutoFim.getValue());
            String IdEquip = txtIdEquipamento.getText().trim();
            Equipamento equip = null;
            int cont = 0;
            for (Equipamento equipamento : e) {
                cont++;
                if (IdEquip.equals(equipamento.getEnderecoLogico())) {
                    equip = new Equipamento(equipamento.getEnderecoLogico(), equipamento.getEnderecoFisico(),
                            equipamento.getDescricao(), equipamento.getFichConfig(), equipamento.getMovimento(), equipamento.getFabricante());
                    break;
                } else {
                    if (cont == e.size()) {
                        throw new EquipamentoException();
                    }
                }
            }
            if (!"null:null".equals(horaInicio) || !"null:null".equals(horaFim)) {
                if (!testeHora(horaInicio, horaFim)) {
                    throw new HoraException();
                }
            }
            System.out.println(diaSemana + " ; " + horaInicio + " ; " + horaFim + " ; " + equip.toString());
            PeriodoDeAutorizacao periodo = new PeriodoDeAutorizacao(
                    diaSemana, horaInicio, horaFim, equip);
            lpa.adicionarPeriodo(periodo);
            criarAlerta(Alert.AlertType.INFORMATION, "Adição de novo Periodo","Periodo adicionado com sucesso").show();
            ((Node) event.getSource()).getScene().getWindow().hide();
        } catch (EquipamentoException eqe) {
            criarAlerta(Alert.AlertType.ERROR, "Equipamento Inexistente", "Equipamento Inexistente").show();
        } catch (HoraException he) {
            criarAlerta(Alert.AlertType.ERROR, "Intervalo de tempo inválido", "Intervalo de tempo inválido").show();
        }
    }

    @FXML
    private void sair(ActionEvent event) {
        ((Node) event.getSource()).getScene().getWindow().hide();
    }

    /**
     * Método para criar alerta.
     *
     * @param tipoAlerta o tipo de alerta
     * @param cabecalho o cabeçalho do alerta
     * @param mensagem a mensagem do alerta
     *
     * @return alerta
     */
    private Alert criarAlerta(Alert.AlertType tipoAlerta, String cabecalho,
            String mensagem) {
        Alert alerta = new Alert(tipoAlerta);

        alerta.setTitle("Aplicação");
        alerta.setHeaderText(cabecalho);
        alerta.setContentText(mensagem);

        return alerta;
    }

    /**
     * Método que associa o equipamento à lista de equipamentos.
     *
     * @param eq lista de equipamentos
     */
    public void associarEquipamentos(List<Equipamento> eq) {
        this.e = eq;
        atualizaTextBoxRegistoEquip();
    }

    /**
     * Método que atualiza a textbox do registo dos equipamentos.
     */
    private void atualizaTextBoxRegistoEquip() {
        String equip = "";
        for (Equipamento equipamento : e) {
            String listaEquipamentosString = equipamento.toString();
            equip = equip + listaEquipamentosString;
        }
        txtMostrarEquipamentos.setText(equip);
    }

    private boolean testeHora(String horaInicio, String horaFim) {
        String[] hInicio = horaInicio.split(":");
        String[] hFim = horaFim.split(":");
        if (Integer.parseInt(hInicio[0]) > Integer.parseInt(hFim[0])) {
            return false;
        } else {
            if (Integer.parseInt(hInicio[0]) == Integer.parseInt(hFim[0])) {
                if (Integer.parseInt(hInicio[1]) > Integer.parseInt(hFim[1])) {
                    return false;
                }
                return true;
            }
        }
        return true;
    }

    @FXML
    public void adicionarPermicaoTotal(ActionEvent event) {
        String horaInicio = "00:00";
        String horaFim = "23:59";
        int i = 0;
        for (Equipamento equipamento : e) {
            Equipamento equip = new Equipamento(equipamento.getEnderecoLogico(), equipamento.getEnderecoFisico(),
                    equipamento.getDescricao(), equipamento.getFichConfig(), equipamento.getMovimento(), equipamento.getFabricante());
            for (i=0; i < DiasSemanaList.size(); i++) {
                String diaSemana = DiasSemanaList.get(i);
                System.out.println(diaSemana + " ; " + horaInicio + " ; " + horaFim + " ; " + equip.toString());
                PeriodoDeAutorizacao periodo = new PeriodoDeAutorizacao(
                        diaSemana, horaInicio, horaFim, equip);
                lpa.adicionarPeriodo(periodo);
            }
            i=0;
        }
        List<PerfilDeAutorizacao> lpa = adicionarPerfil(event);
        exportarFich();
    }

    @FXML
    public void adicionarPermicaoNula(ActionEvent event) {
        String horaInicio = null;
        String horaFim = null;
        String diaSemana = null;
        Equipamento equip = new Equipamento();
        System.out.println(diaSemana + " ; " + horaInicio + " ; " + horaFim + " ; " + equip.toString());
        PeriodoDeAutorizacao periodo = new PeriodoDeAutorizacao(
                diaSemana, horaInicio, horaFim, equip);
        lpa.adicionarPeriodo(periodo);
        List<PerfilDeAutorizacao> lpa = adicionarPerfil(event);
        exportarFich();
    }

    @FXML
    private List<PerfilDeAutorizacao> adicionarPerfil(ActionEvent event) {
        try {
            a = rpController.enviarId();
            b = rpController.enviarDescricao();
            PerfilDeAutorizacao perfil = new PerfilDeAutorizacao(
                    a, b, lpa.getListaPeriodos());
            boolean adicionou = rpa.adicionarPerfil(perfil);
            criarAlerta(Alert.AlertType.INFORMATION, "Adição de novo Perfil", adicionou ? "Perfil adicionado com sucesso"
                    : "Não foi possível adicionar o Perfil").show();
            ((Node) event.getSource()).getScene().getWindow().hide();
        } catch (IllegalArgumentException iae) {
            criarAlerta(Alert.AlertType.ERROR, "", iae.getMessage()).show();
        }
        return rpa.getListaPerfis();
    }

    private void exportarFich(){
        FileChooser flChooser = FileChooserRegistoPerfis.criarFileChooserRegistoPerfis();
        File ficheiroExportar = new File("perfis.ltf");
        FicheiroRegistoPerfis frp = new FicheiroRegistoPerfis();
        if (frp.guardar(ficheiroExportar, rpa)) {
                criarAlerta(Alert.AlertType.INFORMATION, "Exportar Lista",
                        "Contactos exportados com sucesso.").show();
            } else {
                criarAlerta(Alert.AlertType.ERROR, "Exportar Lista",
                        "Problema a exportar a lista!").show();
            }
    }
    
}
