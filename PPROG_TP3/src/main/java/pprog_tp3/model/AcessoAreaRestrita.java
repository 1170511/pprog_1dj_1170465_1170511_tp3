package pprog_tp3.model;

/**
 * Representa o acesso à área restrita.
 * 
 * @author André e Carlos
 */
public class AcessoAreaRestrita {

    /**
     * O equipamento utilizado para o acesso.
     */
    private Equipamento eq;
    
    /**
     * O cartão utilizado para o acesso.
     */
    private String cart;
    
    /**
     * O colaborador que realiza o acesso.
     */
    private String colab;
    
    /**
     * A data do acesso.
     */
    private String data;

    /**
     * A hora do acesso.
     */
    private String hora;

    /**
     * O movimento (entrada/saída)
     */
    private String movimento;

    /**
     * O sucesso (ou não) do acesso
     */
    private String sucesso;

    /**
     * O motivo do acesso
     */
    private String motivo;

    /**
     * Constrói uma instância de AcessoAreaRestrita recebendo por parâmetro o 
     * equipamento, o cartão, o colaborador, a data, a hora, o movimento, 
     * sucesso e o motivo.
     * 
     * @param eq o equipamento utilizado para o acesso
     * @param cart o cartão utilizado para o acesso
     * @param colab o colaborador que realiza o acesso
     * @param data a data do acesso
     * @param hora a hora do acesso
     * @param movimento entrada/saída
     * @param sucesso sucesso (ou não)
     * @param motivo motivo do acesso
     */
    public AcessoAreaRestrita(Equipamento eq, String cart, String colab, String data, String hora, String movimento, String sucesso, String motivo) {
        this.setEquipamento(eq);
        this.setCartao(cart);
        this.setColaborador(colab);
        this.setData(data);
        this.setHora(hora);
        this.setMovimento(movimento);
        this.setSucesso(sucesso);
        this.setMotivo(motivo);
    }
    
    /**
     * Constrói uma instância de AcessoAreaRestrita recebendo por parâmetro o 
     * equipamento, o cartão, o colaborador, a data, a hora, o movimento e o
     * sucesso.
     * 
     * @param eq o equipamento utilizado para o acesso
     * @param cart o cartão utilizado para o acesso
     * @param colab o colaborador que realiza o acesso
     * @param data a data do acesso
     * @param hora a hora do acesso
     * @param movimento entrada/saída
     * @param sucesso sucesso (ou não)
     */
    public AcessoAreaRestrita(Equipamento eq, String cart, String colab, String data, String hora, String movimento, String sucesso) {
        this.setEquipamento(eq);
        this.setCartao(cart);
        this.setColaborador(colab);
        this.setData(data);
        this.setHora(hora);
        this.setMovimento(movimento);
        this.setSucesso(sucesso);
    }

    /**
     * Constrói uma instância de AcessoAreaRestrita (construtor vazio).
     */
    public AcessoAreaRestrita() {
    }
    
    /**
     * Constrói uma instância de AcessoAreaRestrita com as mesmas 
     * características do AcessoAreaRestrita recebida por parâmetro.
     * 
     * @param a o acesso à área restrita com as características a copiar
     */
    public AcessoAreaRestrita(AcessoAreaRestrita a) {
        this.setEquipamento(a.getEquipamento());
        this.setCartao(a.getCartao());
        this.setColaborador(a.getColaborador());
        this.setData(a.getData());
        this.setHora(a.getHora());
        this.setMovimento(a.getMovimento());
        this.setSucesso(a.getSucesso());
        this.setMotivo(a.getMotivo());
    }

    /**
     * Devolve o equipamento.
     * 
     * @return o equipamento
     */
    public Equipamento getEquipamento() {
        return eq;
    }

    /**
     * Modifica o equipamento.
     * 
     * @param eq novo equipamento
     */
    public void setEquipamento(Equipamento eq) {
        this.eq = eq;
    }

    /**
     * Devolve o cartáo utilizado.
     * 
     * @return o cartão
     */
    public String getCartao() {
        return cart;
    }

    /**
     * Modifica o cartão.
     * 
     * @param cart o novo cartão
     */
    public void setCartao(String cart) {
        this.cart = cart;
    }

    /**
     * Devolve o colaborador.
     * 
     * @return o colaborador
     */
    public String getColaborador() {
        return colab;
    }

    /**
     * Modifica o colaborador.
     * 
     * @param colab o novo colaborador
     */
    public void setColaborador(String colab) {
        this.colab = colab;
    }    
    
    /**
     * Devolve a data.
     * 
     * @return a data
     */
    public String getData() {
        return data;
    }

    /**
     * Modifica a data.
     * 
     * @param data nova data
     */
    public void setData(String data) {
        this.data = data;
    }

    /**
     * Devolve a hora.
     * 
     * @return a hora
     */
    public String getHora() {
        return hora;
    }

    /**
     * Modifica a hora.
     * 
     * @param hora nova hora
     */
    public void setHora(String hora) {
        this.hora = hora;
    }

    /**
     * Devolve o movimento (entrada/saída).
     * 
     * @return movimento
     */
    public String getMovimento() {
        return movimento;
    }

    /**
     * Modifica o movimento.
     * 
     * @param movimento o novo movimento
     */
    public void setMovimento(String movimento) {
        this.movimento = movimento;
    }

    /**
     * Devolve o sucesso da operação.
     * 
     * @return sucesso
     */
    public String getSucesso() {
        return sucesso;
    }

    /**
     * Modifica o sucesso da operação.
     * 
     * @param sucesso sucesso ou insucesso da operação
     */
    public void setSucesso(String sucesso) {
        this.sucesso = sucesso;
    }

    /**
     * Devolve o motivo do acesso.
     * 
     * @return o motivo
     */
    public String getMotivo() {
        return motivo;
    }

    /**
     * Modifica o motivo do acesso.
     * 
     * @param motivo o novo motivo do acesso
     */
    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }
    
    
}
