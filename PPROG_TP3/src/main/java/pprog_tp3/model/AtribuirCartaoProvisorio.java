/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pprog_tp3.model;

/**
 * Representa a atribuição do cartão provisório.
 * 
 * @author ANDRÉ E CARLOS
 */
public class AtribuirCartaoProvisorio {

    /**
     * O cartão a atribuir.
     */
    private String m_sCartao;
    
    /**
     * O colaborador a quem o cartão será atribuído.
     */
    private String m_sColaborador;
    
    /**
     * A data de início da atribuição.
     */
    private String m_sDataInicio;
    
    /**
     * A data de fim da atribuição.
     */
    private String m_sDataFim;

    /**
     * Constrói uma instância de AtribuirCartaoProvisorio (construtor vazio).
     */
    public AtribuirCartaoProvisorio() {
    }

    /**
     * Constrói uma instância de AtribuirCartaoProvisorio recebendo por 
     * parâmetro o cartão, o colaborador, a data de início e a data de fim.
     * 
     * @param sCartao o cartão a atribuir
     * @param sColaborador o colaborador a quem será atribuído o cartão
     * @param sDataInicio a data de início da atribuição
     * @param sDataFim a data de fim da atribuição
     */
    public AtribuirCartaoProvisorio(String sCartao, String sColaborador, String sDataInicio, String sDataFim) {
        this.setCartao(sCartao);
        this.setColaborador(sColaborador);
        this.setDataInicio(sDataInicio);
        this.setDataFim(sDataFim);
    }

    /**
     * Devolve o cartão
     * 
     * @return cartão
     */
    public String getCartao() {
        return this.m_sCartao;
    }

    /**
     * Modifica o cartão
     * 
     * @param sCartao novo cartão
     */
    public final void setCartao(String sCartao) {
        this.m_sCartao = sCartao;
    }

    /**
     * Devolve o colaborador
     * 
     * @return colaborador
     */
    public String getM_sColaborador() {
        return m_sColaborador;
    }
    
    /**
     * Modifica o colaborador
     * 
     * @param sColaborador novo colaborador
     */
    public final void setColaborador(String sColaborador) {
        this.m_sColaborador = sColaborador;
    }

    /**
     * Devolve a data de início.
     * 
     * @return data de início 
     */
    public String getM_sDataInicio() {
        return m_sDataInicio;
    }

    /**
     * Modifica a data de início.
     * 
     * @param sDataInicio nova data de início
     */
    public final void setDataInicio(String sDataInicio) {
        this.m_sDataInicio = sDataInicio;
    }

    /**
     * Devolve a data de fim.
     * 
     * @return data de fim
     */
    public String getM_sDataFim() {
        return m_sDataFim;
    }

    /**
     * Modifica a data de fim.
     * 
     * @param m_sDataFim nova data de fim
     */
    public void setDataFim(String m_sDataFim) {
        this.m_sDataFim = m_sDataFim;
    }
    
    /**
     * Método que valida a atribuição. Devolve true caso a atribuição seja 
     * válida e false caso não seja válida.
     * 
     * @return true caso seja válida e false caso contrário.
     */
    public boolean valida() {
        // Escrever aqui o código de validação

        //
        return true;
    }

    /**
     * Método que define como o cartão é identificável (pelo id).
     * 
     * @param sID id do cartão
     * 
     * @return true caso o id seja igual, false caso não seja.
     */
    public boolean isIdentifiableAs(String sID) {
        return this.m_sCartao.equals(sID);
    }

    /**
     * Descrição textual da atribuição do cartão provisório.
     * 
     * @return características da atribuição do cartão provisório
     */
    @Override
    public String toString() {
        return this.m_sCartao + ";" + this.m_sColaborador + ";" + this.m_sDataInicio + ";" + this.m_sDataFim;
    }
}
