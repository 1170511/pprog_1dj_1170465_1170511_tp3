/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pprog_tp3.model;

/**
 * Representa o cartão de identificação.
 *
 * @author ANDRÉ E CARLOS
 */
public class CartaoIdentificacao {

    /**
     * O número de identificação do cartão.
     */
    private String m_sNId;

    /**
     * A data de emissão.
     */
    private String m_sDataEm;

    /**
     * A versão do cartão.
     */
    private String m_sVersao;

    /**
     * A categoria do cartão.
     */
    private String m_sCategoria;

    /**
     * Constrói uma instância de CartaoDeIdentificacao (construtor vazio).
     */
    public CartaoIdentificacao() {
    }

    /**
     * Constrói uma instância de CartaoDeIdentificacao recebendo por parâmetro o
     * número de identificação, a data de emissão, a versão e a categoria.
     *
     * @param sNId número de identificação
     * @param sDataEm data de emissão
     * @param sVersao versão
     * @param sCategoria categoria
     */
    public CartaoIdentificacao(String sNId, String sDataEm, String sVersao, String sCategoria) {
        this.setNumeroId(sNId);
        this.setDataEmissao(sDataEm);
        this.setVersao(sVersao);
        this.setCategoria(sCategoria);
    }

    /**
     * Devolve o número de identificação.
     *
     * @return número de identificação
     */
    public String getNumeroId() {
        return this.m_sNId;
    }

    /**
     * Modifica o número de identificação.
     *
     * @param sNId novo número de identificação
     */
    public final void setNumeroId(String sNId) {
        this.m_sNId = sNId;
    }

    /**
     * Devolve a data de emissão do cartão.
     *
     * @return data de emissão
     */
    public String getDataEm() {
        return m_sDataEm;
    }

    /**
     * Modifica a data de emissão do cartão.
     * 
     * @param sDataE nova data de emissão
     */
    public final void setDataEmissao(String sDataE) {
        this.m_sDataEm = sDataE;
    }

    /**
     * Devolve a categoria do cartão.
     *
     * @return categoria
     */
    public String getCategoria() {
        return m_sCategoria;
    }

    /**
     * Modifica a categoria.
     *
     * @param m_sCategoria nova categoria
     */
    public void setCategoria(String m_sCategoria) {
        this.m_sCategoria = m_sCategoria;
    }

    /**
     * Devolve a versão do cartão.
     * 
     * @return versão
     */
    public String getVersao() {
        return m_sVersao;
    }
    
    /**
     * Modifica a versão do cartão.
     * 
     * @param sVersao nova versão
     */
    public final void setVersao(String sVersao) {
        this.m_sVersao = sVersao;
    }

    /**
     * Devolve true caso o cartão seja válido e false caso não seja.
     * 
     * @return true caso seja válido e false caso contrário.
     */
    public boolean valida() {
        // Escrever aqui o código de validação

        //
        return true;
    }

    /**
     * Método que define como o cartão é identificável (pelo id).
     * 
     * @param sID id do cartão
     * 
     * @return true caso o id seja igual, false caso não seja.
     */
    public boolean isIdentifiableAs(String sID) {
        return this.m_sNId.equals(sID);
    }

    /**
     * Descrição textual do cartão de identificação.
     * 
     * @return características do cartão de identificação
     */
    @Override
    public String toString() {
        return this.m_sNId + ";" + this.m_sDataEm + ";" + this.m_sVersao;
    }
}
