/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pprog_tp3.model;

/**
 * Representa o colaborador.
 * 
 * @author André e Carlos
 */
public class Colaborador {

    /**
     * O número mecanográfico do colaborador.
     */
    private String numMecanografico;

    /**
     * O nome completo do colaborador.
     */
    private String nomeCompleto;

    /**
     * O nome abreviado do colaborador.
     */
    private String nomeAbreviado;

    /**
     * Constrói uma instância de Colaborador (construtor vazio).
     */
    public Colaborador() {
    }

    /**
     * Constrói uma instância de Colaborador recebendo por parâmetro o número 
     * mecanográfico, o nome completo e o nome abreviado.
     * 
     * @param numMecanografico o número mecanográfico
     * @param nomeCompleto o nome completo
     * @param nomeAbreviado o nome abreviado
     */
    public Colaborador(String numMecanografico, String nomeCompleto, String nomeAbreviado) {
        this.setNumMecanografico(numMecanografico);
        this.setNomeCompleto(nomeCompleto);
        this.setNomeAbreviado(nomeAbreviado);
    }
    
    /**
     * Constrói uma instância de Colaborador com as mesmas características do
     * colaborador recebido por parâmetro.
     * 
     * @param c o colaborador com as características a copiar
    */
    public Colaborador(Colaborador c){
        this.setNumMecanografico(c.getNumMecanografico());
        this.setNomeCompleto(c.getNomeCompleto());
        this.setNomeAbreviado(c.getNomeAbreviado());
    }

    /**
     * Devolve o número mecanográfico.
     * 
     * @return número mecanográfico
     */
    public String getNumMecanografico() {
        return numMecanografico;
    }

    /**
     * Modifica o número mecanográfico.
     * 
     * @param numMecanografico novo número mecanográfico
     */
    public void setNumMecanografico(String numMecanografico) {
        this.numMecanografico = numMecanografico;
    }

    /**
     * Devolve o nome completo.
     * @return  nomeCompleto
     */
    public String getNomeCompleto() {
        return nomeCompleto;
    }

    /**
     * Modifica o nome completo.
     * 
     * @param nomeCompleto novo nome completo
     */
    public void setNomeCompleto(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }

    /**
     * Devolve o nome abreviado.
     * 
     * @return novo nome abreviado
     */
    public String getNomeAbreviado() {
        return nomeAbreviado;
    }

    /**
     * Modifica o nome abreviado.
     * 
     * @param nomeAbreviado novo nome abreviado
     */
    public void setNomeAbreviado(String nomeAbreviado) {
        this.nomeAbreviado = nomeAbreviado;
    }
    
    /**
     * Devolve o perfil de autorização.
     * 
     * @return perfil de autorização
     */
    public PerfilDeAutorizacao getPerfil(){
        return new PerfilDeAutorizacao();
    }

}
