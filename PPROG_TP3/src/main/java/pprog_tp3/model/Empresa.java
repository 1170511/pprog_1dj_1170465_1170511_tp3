package pprog_tp3.model;

/**
 * Representa a empresa.
 * 
 * @author André e Carlos
 */
public class Empresa {
    
    /**
     * Registo perfis de autorização.
     */
    private static RegistoPerfisDeAutorizacao rp = new RegistoPerfisDeAutorizacao();
    
    /**
     * Registo de equipamentos.
     */
    private static RegistoDeEquipamentos re = new RegistoDeEquipamentos();
    
    /**
     * Registo de cartões. 
     */
    private static RegistoDeCartoes rc = new RegistoDeCartoes();
    
    /**
     * Registo de acessos.
     */
    private static RegistoDeAcessos ra = new RegistoDeAcessos();
    
    /**
     * Devolve o registo de equipamentos.
     * 
     * @return registo de equipamentos
     */
    public static RegistoDeEquipamentos getRegistoEquipamentos(){
        return re;
    }
    
    /**
     * Devolve o registo de perfis.,
     * 
     * @return registo de perfis
     */
    public static RegistoPerfisDeAutorizacao getRegistoPerfis(){
        return rp;
    }  
    
    /**
     * Devolve o registo de cartões.
     * 
     * @return registo de cartões
     */
    public static RegistoDeCartoes getRegistoCartoes() {
        return rc;
    }
    
    /**
     * Devolve o registo de acessos.
     * 
     * @return registo de acessos
     */
    public static RegistoDeAcessos getRegistoAcessos() {
        return ra;
    }
}
