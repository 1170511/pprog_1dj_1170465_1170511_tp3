package pprog_tp3.model;

/**
 * Representa um equipamento através do endereço lógico, endereço físico, 
 * descrição, ficheiro de configuração, movimento e fabricante.
 * 
 * @author André e Carlos
 */
public class Equipamento {

    /**
     * O endereço lógico.
     */
    private String enderecoLogico;

    /**
     * O endereço físico.
     */
    private String enderecoFisico;

    /**
     * A descrição.
     */
    private String descricao;

    /**
     * O nome do ficheiro de configuração.
     */
    private String fichConfig;

    /**
     * O movimento.
     */
    private String movimento;
    
    /**
     * O nome do fabricante.
     */
    private String fabricante;

    /**
     * Constrói uma instância de Equipamento recebendo por parâmetro o endereço
     * lógico, o endereço físico, a descrição, o ficheiro de configuração, o 
     * movimento e o fabricante.
     * 
     * @param enderecoLogico o endereço lógico
     * @param enderecoFisico o endereço físico
     * @param descricao a descrição
     * @param fichConfig o ficheiro de configuração
     * @param movimento o movimento
     * @param fabricante o fabricante
     */
    public Equipamento(String enderecoLogico, String enderecoFisico, String descricao, String fichConfig, String movimento, String fabricante) {
        this.setEnderecoLogico(enderecoLogico);
        this.setEnderecoFisico(enderecoFisico);
        this.setDescricao(descricao);
        this.setFichConfig(fichConfig);
        this.setMovimento(movimento);
        this.setFabricante(fabricante);
    }

    /**
     * Constrói uma instância de Equipamento com as mesmas características do
     * equipamento recebido por parâmetro.
     * 
     * @param e o equipamento com as características a copiar
     */
    public Equipamento(Equipamento e) {
        this.setEnderecoLogico(e.getEnderecoLogico());
        this.setEnderecoFisico(e.getEnderecoFisico());
        this.setDescricao(e.getDescricao());
        this.setFichConfig(e.getFichConfig());
        this.setMovimento(e.getMovimento());
        this.setFabricante(e.getFabricante());
    }

    /**
     * Constrói uma instância de Equipamento (construtor vazio).
     */
    public Equipamento() {
    } 

    /**
     * Devolve o endereço lógico.
     * 
     * @return endereço lógico
     */
    public String getEnderecoLogico() {
        return enderecoLogico;
    }

    /**
     * Modifica o endereço lógico.
     * 
     * @param enderecoLogico novo endereço lógico
     */
    public void setEnderecoLogico(String enderecoLogico) {
        this.enderecoLogico = enderecoLogico;
    }

    /**
     * Devolve o endereço físico.
     * 
     * @return endereço físico
     */
    public String getEnderecoFisico() {
        return enderecoFisico;
    }

    /**
     * Modifica o endereço físico.
     * 
     * @param enderecoFisico novo endereço físico
     */
    public void setEnderecoFisico(String enderecoFisico) {
        this.enderecoFisico = enderecoFisico;
    }

    /**
     * Devolve a descrição.
     * 
     * @return a descrição
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Modifica a descrição.
     * 
     * @param descricao a nova descrição
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * Devolve o nome do ficheiro de configuração.
     * 
     * @return o nome do ficheiro de configuração
     */
    public String getFichConfig() {
        return fichConfig;
    }

    /**
     * Modifica o nome do ficheiro de configuração
     * 
     * @param fichConfig novo nome do ficheiro de configuração
     */
    public void setFichConfig(String fichConfig) {
        this.fichConfig = fichConfig;
    }

    /**
     * Devolve o movimento.
     * 
     * @return o movimento
     */
    public String getMovimento() {
        return movimento;
    }

    /**
     * Modifica o movimento.
     * 
     * @param movimento o novo movimento
     */
    public void setMovimento(String movimento) {
        this.movimento = movimento;
    }
    
    /**
     * Devolve o fabricante.
     * 
     * @return o fabricante
     */
    public String getFabricante() {
        return fabricante;
    }

    /**
     * Modifica o fabricante.
     * 
     * @param fabricante o novo fabricante
     */
    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }

    /**
     * Devolve true caso o equipamento seja válido e false caso não seja.
     * 
     * @return true caso seja válido e false caso contrário.
     */
    public boolean valida() {
        // Escrever aqui o código de validação

        //
        return true;
    }

    /**
     * Método que define como o equipamento é identificável (pelo id).
     * 
     * @param ID id do equipamento
     * 
     * @return true caso o endereço lógico seja igual ao id, false caso não seja.
     */
    public boolean isIdentifiableAs(String ID) {
        return this.enderecoLogico.equals(ID);
    }
    
    /**
     * Descrição textual do equipamento.
     *
     * @return características do equipamento
     */
    @Override
    public String toString() {
        return this.enderecoLogico + ";" + this.enderecoFisico + ";" + this.descricao + ";" + this.fichConfig + ";" + this.movimento+ ";" + this.fabricante + "\n";
    }

    /**
     * Devolve a área restrita.
     * 
     * @return área restrita
     */
    public AreaRestrita getAreaRestrita() {
        return new AreaRestrita();
    }
}
