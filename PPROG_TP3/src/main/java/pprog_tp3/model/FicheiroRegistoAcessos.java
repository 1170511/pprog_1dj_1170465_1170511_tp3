package pprog_tp3.model;

import java.io.*;

/**
 * Ficheiro Registo de Acessos.
 * 
 * @author André e Carlos
 */
public class FicheiroRegistoAcessos {
    
    /**
     * Nome do ficheiro.
     */
    public static final String NOME_FICHEIRO = "ListaAcessos.ltf";

    /**
     * Construtor vazio.
     */
    public FicheiroRegistoAcessos() {
    }

    /**
     * Leitura do ficheiro.
     * 
     * @return lista de acessos
     */
    public RegistoDeAcessos ler() {
        return ler(new File(NOME_FICHEIRO));
    }

    /**
     * Leitura do ficheiro.
     * 
     * @param nomeFicheiro o nome do ficheiro
     * 
     * @return lista de acessos
     */
    public RegistoDeAcessos ler(String nomeFicheiro) {
        return ler(new File(nomeFicheiro));
    }

    /**
     * Leitura do ficheiro.
     * 
     * @param ficheiro o nome do ficheiro
     * 
     * @return lista de acessos
     */
    public RegistoDeAcessos ler(File ficheiro) {
        RegistoDeAcessos listaAcessos;
        try {
            ObjectInputStream in = new ObjectInputStream(
                    new FileInputStream(ficheiro));
            try {
                listaAcessos = (RegistoDeAcessos) in.readObject();
            } finally {
                in.close();
            }
            return listaAcessos;
        } catch (IOException | ClassNotFoundException ex) {
            return new RegistoDeAcessos();
        }
    }

    /**
     * Guarda o ficheiro.
     * 
     * @param listaAcessos a lista de acessos
     * 
     * @return true caso a lista seja guardada e false caso contrário
     */
    public boolean guardar(RegistoDeAcessos listaAcessos) {
        return guardar(new File(NOME_FICHEIRO), listaAcessos);
    }
    
    /**
     * Guarda o ficheiro.
     * 
     * @param nomeFicheiro o nome do ficheiro
     * @param listaAcessos a lista de acessos
     * 
     * @return true caso a lista seja guardada e false caso contrário
     */
    public boolean guardar(String nomeFicheiro, RegistoDeAcessos listaAcessos) {
        return guardar(new File(nomeFicheiro), listaAcessos);
    }

    /**
     * Guarda o ficheiro.
     * 
     * @param ficheiro o nome do ficheiro
     * @param listaAcessos a lista de acessos
     * 
     * @return true caso a lista seja guardada e false caso contrário
     */
    public boolean guardar(File ficheiro, RegistoDeAcessos listaAcessos) {
        try {
            ObjectOutputStream out = new ObjectOutputStream(
                    new FileOutputStream(ficheiro));
            try {
                out.writeObject(listaAcessos);
            } finally {
                out.close();
            }
            return true;
        } catch (IOException ex) {
            return false;
        }
    }
    
}
