package pprog_tp3.model;

import java.io.*;

/**
 * Ficheiro Regitso de Perfis.
 * 
 * @author André e Carlos
 */
public class FicheiroRegistoPerfis {
    
    /**
     * O nome do ficheiro.
     */
    public static final String NOME_FICHEIRO = "ListaPerfis.ltf";

    /**
     * Construtor vazio.
     */
    public FicheiroRegistoPerfis() {
    }

    /**
     * Leitura do ficheiro.
     * 
     * @return lista de perfis
     */
    public RegistoPerfisDeAutorizacao ler() {
        return ler(new File(NOME_FICHEIRO));
    }

    /**
     * Leitura do ficheiro.
     * 
     * @param nomeFicheiro o nome do ficheiro
     * 
     * @return lista de perfis
     */
    public RegistoPerfisDeAutorizacao ler(String nomeFicheiro) {
        return ler(new File(nomeFicheiro));
    }

    /**
     * Leitura do ficheiro.
     * 
     * @param ficheiro o nome do ficheiro
     * 
     * @return lista de perfis
     */
    public RegistoPerfisDeAutorizacao ler(File ficheiro) {
        RegistoPerfisDeAutorizacao listaPerfis;
        try {
            ObjectInputStream in = new ObjectInputStream(
                    new FileInputStream(ficheiro));
            try {
                listaPerfis = (RegistoPerfisDeAutorizacao) in.readObject();
            } finally {
                in.close();
            }
            return listaPerfis;
        } catch (IOException | ClassNotFoundException ex) {
            return new RegistoPerfisDeAutorizacao();
        }
    }

    /**
     * Guarda o ficheiro.
     * 
     * @param listaPerfis a lista de perfis
     * 
     * @return true caso a lista seja guardada e false caso contrário
     */
    public boolean guardar(RegistoPerfisDeAutorizacao listaPerfis) {
        return guardar(new File(NOME_FICHEIRO), listaPerfis);
    }
    
    /**
     * Guarda o ficheiro.
     * 
     * @param nomeFicheiro o nome do ficheiro
     * @param listaPerfis a lista de perfis
     * 
     * @return true caso a lista seja guardada e false caso contráriov
     */
    public boolean guardar(String nomeFicheiro, RegistoPerfisDeAutorizacao listaPerfis) {
        return guardar(new File(nomeFicheiro), listaPerfis);
    }

    /**
     * Guarda o ficheiro.
     * 
     * @param ficheiro o nome do ficheiro
     * @param listaPerfis a lista de perfis
     * 
     * @return true caso a lista seja guardada e false caso contrário
     */
    public boolean guardar(File ficheiro, RegistoPerfisDeAutorizacao listaPerfis) {
        try {
            ObjectOutputStream out = new ObjectOutputStream(
                    new FileOutputStream(ficheiro));
            try {
                out.writeObject(listaPerfis);
            } finally {
                out.close();
            }
            return true;
        } catch (IOException ex) {
            return false;
        }
    }
    
}
