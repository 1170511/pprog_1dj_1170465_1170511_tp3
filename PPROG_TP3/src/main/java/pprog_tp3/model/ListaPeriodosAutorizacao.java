package pprog_tp3.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Representa uma lista de períodos de autorização.
 * 
 * @author André e Carlos
 */
public class ListaPeriodosAutorizacao implements Serializable {

    /**
     * A lista de períodos.
     */
    public List<PeriodoDeAutorizacao> listaPeriodos;

    /**
     * Constrói uma instância de ListaPeriodosAutorizacao, inicializando a lista
     * de períodos vazia.
     */
    public ListaPeriodosAutorizacao() {
        listaPeriodos = new ArrayList<>();
    }

    /**
     * Constrói uma instância de ListaPeriodosAutorizacao, recebendo por 
     * parâmetro a lista de períodos.
     * 
     * @param listaPeriodos a lista de períodos
     */
    public ListaPeriodosAutorizacao(ListaPeriodosAutorizacao listaPeriodos) {
        this.listaPeriodos = new ArrayList<>(listaPeriodos.getListaPeriodos());
    }

    /**
     * Devolve a lista de períodos.
     * 
     * @return lista de períodos
     */
    public List<PeriodoDeAutorizacao> getListaPeriodos() {
        return new ArrayList<>(listaPeriodos);
    }

    /**
     * Modifica a lista de períodos.
     * 
     * @param listaPeriodos nova lista de períodos
     */
    public void setListaPeriodos(ListaPeriodosAutorizacao listaPeriodos) {
        this.listaPeriodos.clear();
        adicionarListaPeriodos(listaPeriodos);
    }

    /**
     * Devolve o período, procurando o índice do período na lista de períodos.
     * 
     * @param indice o índice do período na lista
     * 
     * @return período a partir do índice
     */
    public PeriodoDeAutorizacao getPeriodo(int indice) {
        return listaPeriodos.get(indice);
    }

    /**
     * Modifica o período contido na lista de períodos e o seu índice.
     * 
     * @param indice o índice do período
     * @param periodo o período a alterar
     * 
     * @return novo período
     */
    public PeriodoDeAutorizacao setPeriodo(int indice, PeriodoDeAutorizacao periodo) {
        if (!listaPeriodos.contains(periodo)) {
            return listaPeriodos.set(indice, periodo);
        }
        throw new IllegalArgumentException("Periodo já existe.");
    }

    /**
     * Verifica se a lista de períodos contém o período. Caso contenha retorna
     * false, caso contrário adiciona o período à lista.
     * 
     * @param periodo o período a adicionar
     * 
     * @return false caso o período já esteja na lista e adiciona caso não 
     * esteja
     */
    public boolean adicionarPeriodo(PeriodoDeAutorizacao periodo) {
        return listaPeriodos.add(periodo);
    }

    /**
     * Devolve o total de períodos adicionados.
     * 
     * @param listaPeriodos a lista de períodos
     * 
     * @return o total de períodos adicionados
     */
    public int adicionarListaPeriodos(ListaPeriodosAutorizacao listaPeriodos) {
        int totalPeriodosAdicionados = 0;
        for (PeriodoDeAutorizacao periodo : listaPeriodos.getListaPeriodos()) {
            boolean PeriodoAdicionado = adicionarPeriodo(periodo);
            if (PeriodoAdicionado) {
                totalPeriodosAdicionados++;
            }
        }
        return totalPeriodosAdicionados;
    }

    /**
     * Remove um período da lista de períodos.
     * 
     * @param periodo o período a remover
     * 
     * @return a lista de períodos alterada
     */
    public boolean removerPeriodo(PeriodoDeAutorizacao periodo) {
        return listaPeriodos.remove(periodo);
    }

    /**
     * Remove um período de autorização a partir do seu índice na lista
     * 
     * @param indice o índice do período na lista
     * 
     * @return a lista de períodos alterada
     */
    public PeriodoDeAutorizacao removerPeriodo(int indice) {
        return listaPeriodos.remove(indice);
    }

    /**
     * Remove todos os períodos da lista de períodos.
     * 
     * @param listaPeriodos a lista de períodos
     * 
     * @return lista de períodos vazia
     */
    public boolean removerTodos(ListaPeriodosAutorizacao listaPeriodos) {
        return this.listaPeriodos.removeAll(listaPeriodos.getListaPeriodos());
    }

    /**
     * Devolve o índice do período contido na lista.
     * 
     * @param periodo o período
     * 
     * @return o índice do período
     */
    public int obterIndicePeriodo(PeriodoDeAutorizacao periodo) {
        return listaPeriodos.indexOf(periodo);
    }

    /**
     * Limpa a lista de períodos.
     */
    public void limpar() {
        listaPeriodos.clear();
    }

    /**
     * Devolve o número de períodos de autorização que a lista contém.
     * 
     * @return o número de períodos de autorização contidos na lista
     */
    public int tamanho() {
        return listaPeriodos.size();
    }

    /**
     * Método que verifica se a lista de períodos está vazia.
     * 
     * @return true caso esteja vazia, false se não estiver.
     */
    public boolean isVazio() {
        return listaPeriodos.isEmpty();
    }

    /**
     * Devolve a lista de períodos em array.
     * 
     * @return array lista de períodos
     */
    public PeriodoDeAutorizacao[] getArray() {
        return listaPeriodos.toArray(new PeriodoDeAutorizacao[listaPeriodos.size()]);
    }

    /**
     * Descrição textual da lista de períodos de autorização.
     *
     * @return características da lista de períodos
     */
    @Override
    public String toString() {
        List<PeriodoDeAutorizacao> copia = new ArrayList<>(listaPeriodos);
        Collections.sort(copia);

        StringBuilder s = new StringBuilder();
        for (PeriodoDeAutorizacao periodo : copia) {
            s.append(periodo);
            s.append("\n");
        }

        return s.toString().trim();
    }

    /**
     * Compara dois objetos em memória.
     * 
     * @param outroObjeto o outro objeto a comparar
     * 
     * @return true caso os dois objetos partilhem referências e false caso não
     * partilhem.
     */
    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (outroObjeto == null || getClass() != outroObjeto.getClass()) {
            return false;
        }
        ListaPeriodosAutorizacao outraListaPeriodos = (ListaPeriodosAutorizacao) outroObjeto;

        List<PeriodoDeAutorizacao> copiaThis = new ArrayList<>(listaPeriodos);
        List<PeriodoDeAutorizacao> copiaOutra = new ArrayList<>(outraListaPeriodos.listaPeriodos);

        return copiaThis.equals(copiaOutra);
    }

    /**
     * Cria um novo período de autorização.
     * 
     * @return novo período de autorização
     */
    public PeriodoDeAutorizacao criaPeriodo() {
        return new PeriodoDeAutorizacao();
    }

    /**
     * Devolve true caso o período seja válido e false caso não seja válido.
     *
     * @return true caso o período seja válido e false caso não seja
     */
    public boolean valida() {
        
        return true;
    }
}
