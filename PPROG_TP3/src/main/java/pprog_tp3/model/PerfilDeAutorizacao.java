package pprog_tp3.model;

import java.util.List;

/**
 * Representa o perfil de autorização através do ID e da descrição.
 *
 * @author André e Carlos
 */
public class PerfilDeAutorizacao {

    /**
     * O ID do perfil
     */
    private String id;

    /**
     * A descrição do perfil.
     */
    private String desc;
    
    private List<PeriodoDeAutorizacao> periodos;

    /**
     * Lista de equipamentos.
     */
    private RegistoDeEquipamentos le = new RegistoDeEquipamentos();

    /**
     * Lista de períodos de autorização.
     */
    private ListaPeriodosAutorizacao lpa = new ListaPeriodosAutorizacao();
    
    private RegistoPerfisDeAutorizacao rpa = new RegistoPerfisDeAutorizacao();
    
    private ListaPeriodosAutorizacao lstPeriodos;

    /**
     * Constrói uma instância de PerfilDeAutorizacao recebendo por parâmetro o
     * id e a descrição.
     * @param id o id do perfil
     * @param desc a descrição
     * @param periodos a lista de periodos
     */
    public PerfilDeAutorizacao(String id, String desc, List<PeriodoDeAutorizacao> periodos) {
        this.setId(id);
        this.setDescricao(desc);
        this.setPeriodo(periodos);
    }

    /**
     * Constrói uma instância de PerfilDeAutorizacao (construtor vazio).
     */
    public PerfilDeAutorizacao() {
        this.lstPeriodos = new ListaPeriodosAutorizacao();
    }

    /**
     * Constrói uma instância de PerfilDeAutorizacao com as mesmas 
     * características que o perfil de autorização passado por parâmetro.
     * 
     * @param perfil o perfil com as características a copiar
     */
    public PerfilDeAutorizacao(PerfilDeAutorizacao perfil) {
        this.setId(perfil.getId());
        this.setDescricao(perfil.getDescricao());
        this.setPeriodo(periodos);
    }

    public List<PeriodoDeAutorizacao> getPeriodo() {
        return periodos;
    }

    public void setPeriodo(List<PeriodoDeAutorizacao> periodos) {
        this.periodos = periodos;
    }
    

    /**
     * Devolve o ID.
     * 
     * @return o ID
     */
    public String getId() {
        return id;
    }

    /**
     * Modifica o ID.
     * 
     * @param id novo ID
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Devolve a descrição.
     * 
     * @return a descrição
     */
    public String getDescricao() {
        return desc;
    }

    /**
     * Modifica a descrição.
     * 
     * @param des nova descrição
     */
    public void setDescricao(String des) {
        this.desc = desc;
    }

    /**
     * Adiciona um novo equipamento à lista de equipamentos.
     * 
     * @param e novo equipamento
     */
    public void addEquipamento(Equipamento e) {
        le.addEquipamento(e);
    }

    /**
     * Retorna true se adicionar o período à lista de períodos de autorização e 
     * false caso não adicione.
     * 
     * @param pa novo periodo de autorização
     * 
     * @return true caso adicione, false caso não adicione
     */
    public boolean addPeriodo(PeriodoDeAutorizacao pa) {
        return lpa.adicionarPeriodo(pa);
    }
    
    public boolean addPerfil(PerfilDeAutorizacao pa) {
        return rpa.adicionarPerfil(pa);
    }

    /**
     * Devolve true caso o perfil de autorização seja válido e false caso não 
     * seja.
     * 
     * @return true caso seja válido e false caso contrário.
     */
    public boolean valida() {
        boolean a = true;

        return a;
    }

    /**
     * Descrição textual do perfil de autorização.
     *
     * @return características do perfil
     */
    @Override
    public String toString() {
        return this.id + ";" + this.desc;
    }

    /**
     * Método que define como o perfil é identificável (pelo id).
     * 
     * @param ID id do perfil
     * 
     * @return true caso o id seja igual, false caso não seja.
     */
    public boolean isIdentifiableAs(String ID) {
        return this.id.equals(ID);
    }

    /**
     * Método que verifica se o equipamento está autorizado. Devolve true se 
     * estiver e false se não estiver.
     * 
     * @param eq o equipamento
     * @param data a data
     * @param hora a hora
     * 
     * @return true caso o equipamento esteja autorizado e false caso não 
     * esteja.
     */
    public boolean isEquipamentoAutorizado(Equipamento eq, String data, String hora) {
        List<PeriodoDeAutorizacao> listaPeriodos = lpa.getListaPeriodos();
        for(PeriodoDeAutorizacao periodo : listaPeriodos) {
            if(periodo.getEquipamento().getEnderecoLogico().equalsIgnoreCase(eq.getEnderecoLogico())){
                if(periodo.getDiaSemana().equalsIgnoreCase(data)){
                    if((converterStringHoraEmIntMinutos(periodo.getHoraInicio()) <= converterStringHoraEmIntMinutos(hora)) &&
                            (converterStringHoraEmIntMinutos(hora)) <= converterStringHoraEmIntMinutos(periodo.getHoraFim())){
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    /**
     * Método que converte a String hora em inteiro minutos
     * 
     * @param hora a hora (string)
     * 
     * @return a hora em minutos (inteiro)
     */
    public static int converterStringHoraEmIntMinutos(String hora){
        String[] parametro = hora.trim().split(":");
        int horaEmMinutos = Integer.parseInt(parametro[0].trim()) * 60 + Integer.parseInt(parametro[1].trim());
        return horaEmMinutos;
    }
}
