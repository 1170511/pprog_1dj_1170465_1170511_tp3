package pprog_tp3.model;

import java.io.Serializable;

/**
 * Representa o período de autorização através do dia da semana, da hora de
 * início, da hora de fim e do equipamento.
 *
 * @author André e Carlos
 */
public class PeriodoDeAutorizacao implements Comparable<PeriodoDeAutorizacao>, Serializable {

    /**
     * O dia da semana.
     */
    private String diaSemana;

    /**
     * A hora de início.
     */
    private String horaInicio;

    /**
     * A hora de fim.
     */
    private String horaFim;

    /**
     * O equipamento.
     */
    private Equipamento equip;
    
    /**
     * Constrói uma instância de PeriodoDeAutorizacao recebendo por parâmetro o
     * dia da semana, a hora de início, a hora de fim e um equipamento.
     *
     * @param diaSemana o dia da semana
     * @param horaInicio a hora de início
     * @param horaFim a hora de fim
     * @param equip o equipamento
     */
    public PeriodoDeAutorizacao(String diaSemana, String horaInicio, String horaFim, Equipamento equip) {
        this.setDiaSemana(diaSemana);
        this.setHoraInicio(horaInicio);
        this.setHoraFim(horaFim);
        this.setEquipamento(equip);
    }

    /**
     * Constrói uma instância de PeriodoDeAutorizacao (construtor vazio).
     */
    public PeriodoDeAutorizacao() {
    }

    /**
     * Constrói uma instância de PeriodoDeAutorizacao com as mesmas
     * características do periodo de autorizacao recebido por parâmetro.
     *
     * @param p o período de autorização com as características a copiar
     */
    public PeriodoDeAutorizacao(PeriodoDeAutorizacao p) {
        this.setDiaSemana(p.getDiaSemana());
        this.setHoraInicio(p.getHoraInicio());
        this.setHoraFim(p.getHoraFim());
        this.setEquipamento(p.getEquipamento());
    }

    /**
     * Devolve o equipamento.
     * 
     * @return o equipamento
     */
    public Equipamento getEquipamento() {
        return equip;
    }

    /**
     * Modifica o equipamento.
     * 
     * @param equip novo equipamento
     */
    public void setEquipamento(Equipamento equip) {
        this.equip = equip;
    }

    /**
     * Devolve o dia da semana.
     * 
     * @return o dia da semana
     */
    public String getDiaSemana() {
        return diaSemana;
    }

    /**
     * Modifica o dia da semana.
     * 
     * @param diaSemana novo dia da semana
     */
    public void setDiaSemana(String diaSemana) {
        this.diaSemana = diaSemana;
    }

    /**
     * Devolve a hora de início.
     * 
     * @return a hora de início
     */
    public String getHoraInicio() {
        return horaInicio;
    }

    /**
     * Modifica a hora de início.
     * 
     * @param horaInicio nova hora de início
     */
    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    /**
     * Devolve a hora de fim.
     * 
     * @return a hora de fim
     */
    public String getHoraFim() {
        return horaFim;
    }

    /**
     * Modifica a hora de fim.
     * 
     * @param horaFim nova hora de fim
     */
    public void setHoraFim(String horaFim) {
        this.horaFim = horaFim;
    }

    /**
     * Descrição textual do período de autorização.
     *
     * @return características do período de autorização
     */
    @Override
    public String toString() {
        return String.format(diaSemana, " - " ,horaInicio, " - " ,horaFim, " - ", equip);
    }

    /**
     * Compara dois objetos em memória.
     * 
     * @param outroObjeto o outro objeto a comparar
     * 
     * @return true caso os dois objetos partilhem referências e false caso não
     * partilhem.
     */
    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (outroObjeto == null || getClass() != outroObjeto.getClass()) {
            return false;
        }
        PeriodoDeAutorizacao outroPeriodo = (PeriodoDeAutorizacao) outroObjeto;

        return this.diaSemana.equalsIgnoreCase(outroPeriodo.diaSemana)
                && horaInicio.equalsIgnoreCase(outroPeriodo.horaInicio)
                && horaFim.equalsIgnoreCase(outroPeriodo.horaFim);
    }

    /**
     * Método utilizado para comparar dois objetos,
     * 
     * @param outroPeriodo o outro período utilizado para comparar
     * 
     * @return a diferença entre as horas de início
     */
    @Override
    public int compareTo(PeriodoDeAutorizacao outroPeriodo) {
        return Integer.parseInt(horaInicio) - Integer.parseInt(outroPeriodo.horaInicio);
    }

}
