package pprog_tp3.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Representa o registo/lista de acessos.
 * 
 * @author André e Carlos
 */
public class RegistoDeAcessos {
    
    /**
     * A lista de acessos.
     */
    private final List<AcessoAreaRestrita> listaAcessos;
    
    /**
     * Constrói uma instância de RegistoDeAcessos, inicializando a lista de 
     * acessos vazia
     */
    public RegistoDeAcessos() {
        this.listaAcessos = new ArrayList<AcessoAreaRestrita>();
    }
    
    /**
     * Cria um novo acesso à área restrita.
     * 
     * @param e equipamento 
     * @param cart cartão
     * @param colab colaborador
     * @param data data
     * @param hora hora
     * @param sucesso sucesso
     * @param movimento movimento
     * 
     * @return novo acesso criado
     */
    public AcessoAreaRestrita novoAcesso(Equipamento e, String cart, String colab, String data, String hora, String sucesso, String movimento){
        return new AcessoAreaRestrita(e,cart,colab,data,hora,sucesso,movimento);
    }
    
    public void addAcesso(AcessoAreaRestrita a){
        listaAcessos.add(a);
    }
    
}
