package pprog_tp3.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Representa o registo/lista de cartões.
 * 
 * @author André e Carlos
 */
public class RegistoDeCartoes {

    /**
     * A lista de cartões de identificação.
     */
    private final List<CartaoIdentificacao> m_lstCartoes;
    
    /**
     * A lista de cartóes definitivos.
     */
    private final List<AtribuirCartaoDefinitivo> m_lstAtribuirCartoesDefinitivos;
    
    /**
     * A lista de cartões provisórios.
     */
    private final List<AtribuirCartaoProvisorio> m_lstAtribuirCartoesProvisorios;

    /**
     * Constrói uma instância de RegistoDeCartoes, inicializando todas as listas
     * de cartões vazias.
     */
    public RegistoDeCartoes() {
        this.m_lstCartoes = new ArrayList<CartaoIdentificacao>();
        this.m_lstAtribuirCartoesDefinitivos = new ArrayList<AtribuirCartaoDefinitivo>();
        this.m_lstAtribuirCartoesProvisorios = new ArrayList<AtribuirCartaoProvisorio>();
    }

    public CartaoIdentificacao novoCartao() {
        return new CartaoIdentificacao();
    }

    /**
     * Devolve true caso o cartão seja válido e false caso não seja válido.
     *
     * @param c cartão a validar
     *
     * @return true caso o cartão seja válido e false caso não seja
     */
    public boolean validaCartao(CartaoIdentificacao c) {
        boolean bRet = false;
//        if (c.valida()) {
//             Escrever aqui o código de validação
//
//            
//            bRet = true;
//        }
        return bRet;
    }

    /**
     * Método que faz a validação do cartão e, caso este seja válido,
     * adiciona-o e retorna true, caso contrário não adiciona e retorna
     * false.
     *
     * @param c cartão a registar
     *
     * @return true caso seja válido e adicione, false caso contrário
     */
    public boolean registaCartao(CartaoIdentificacao c) {
        if (this.validaCartao(c)) {
            return addCartao(c);
        }
        return false;
    }

    /**
     * Adiciona um cartão à lista de cartões.
     * 
     * @param c o cartão a adicionar
     * 
     * @return true caso adicione
     */
    private boolean addCartao(CartaoIdentificacao c) {
        return m_lstCartoes.add(c);
    }

    /**
     * Devolve a lista de cartões.
     * 
     * @return a lista de cartões
     */
    public List<CartaoIdentificacao> getListaCartoes() {
        return this.m_lstCartoes;
    }

    /**
     * Devolve o cartão.
     * 
     * @param cartao o cartão
     * 
     * @return o cartão
     */
    public CartaoIdentificacao getCartao(String cartao) {
        for (CartaoIdentificacao term : this.m_lstCartoes) {
            if (term.isIdentifiableAs(cartao)) {
                return term;
            }
        }
        return null;
    }
    
    /**
     * Devolve o cartão pelo id.
     * 
     * @param idCartao o id do cartão
     * 
     * @return o cartão
     */
    public CartaoIdentificacao getCartaoById(String idCartao) {
        for (CartaoIdentificacao c : this.m_lstCartoes) {
            if (c.isIdentifiableAs(idCartao)) {
                return c;
            }
        }

        return null;
    }

    public AtribuirCartaoDefinitivo novaAtribuicaoDefinitivo() {
        return new AtribuirCartaoDefinitivo();
    }

    /**
     * Devolve true caso a atribuição do cartão seja válida e false caso não 
     * seja válida.
     *
     * @param acd a atribuição a validar
     *
     * @return true caso a atribuição seja válida e false caso não seja
     */
    public boolean validaAtribuicaoCartao(AtribuirCartaoDefinitivo acd) {
        boolean bRet = false;
        if (acd.valida()) {
            // Escrever aqui o código de validação

            //
            bRet = true;
        }
        return bRet;
    }

    /**
     * Método que faz a validação da atribuição do cartão e, caso esta seja 
     * válida adiciona-a e retorna true, caso contrário não adiciona e retorna
     * false.
     *
     * @param acd atribuição a adicionar
     *
     * @return true caso seja válida e adicione, false caso contrário
     */
    public boolean atribuirCartaoDefinitivo(AtribuirCartaoDefinitivo acd) {
        if (this.validaAtribuicaoCartao(acd)) {
            return addAtribuicao(acd);
        }
        return false;
    }

    /**
     * Adiciona uma atribuição à lista de atribuições de cartões definitivos.
     * 
     * @param acd atribuição de cartão definitivo
     * 
     * @return true caso adicione
     */
    private boolean addAtribuicao(AtribuirCartaoDefinitivo acd) {
        return m_lstAtribuirCartoesDefinitivos.add(acd);
    }

    /**
     * Devolve a lista de atribuições de cartões definitivos.
     * 
     * @return a lista de atribuições de cartões definitivos
     */
    public List<AtribuirCartaoDefinitivo> getListaAtribuicaoCartoesDefinitivos() {
        return this.m_lstAtribuirCartoesDefinitivos;
    }

    /**
     * Devolve a atribuição do cartão definitivo.
     * 
     * @param sAtribuirCartaoDefinitivo a atribuição do cartão definitivo
     * 
     * @return a atribuição do cartão definitivo
     */
    public AtribuirCartaoDefinitivo getAtribuirCartaoDefinitivo(String sAtribuirCartaoDefinitivo) {
        for (AtribuirCartaoDefinitivo term : this.m_lstAtribuirCartoesDefinitivos) {
            if (term.isIdentifiableAs(sAtribuirCartaoDefinitivo)) {
                return term;
            }
        }

        return null;
    }

    public AtribuirCartaoProvisorio novaAtribuicaoProvisorio() {
        return new AtribuirCartaoProvisorio();
    }

    /**
     * Devolve true caso a atribuição do cartão seja válida e false caso não 
     * seja válida.
     *
     * @param acp a atribuição a validar
     *
     * @return true caso a atribuição seja válida e false caso não seja
     */
    public boolean validaAtribuicaoCartao(AtribuirCartaoProvisorio acp) {
        boolean bRet = false;
        if (acp.valida()) {
            // Escrever aqui o código de validação

            //
            bRet = true;
        }
        return bRet;
    }

    /**
     * Método que faz a validação da atribuição do cartão e, caso esta seja 
     * válida adiciona-a e retorna true, caso contrário não adiciona e retorna
     * false.
     *
     * @param acp atribuição a adicionar
     *
     * @return true caso seja válida e adicione, false caso contrário
     */
    public boolean atribuirCartaoProvisorio(AtribuirCartaoProvisorio acp) {
        if (this.validaAtribuicaoCartao(acp)) {
            return addAtribuicao(acp);
        }
        return false;
    }

    /**
     * Adiciona uma atribuição à lista de atribuições de cartões provisórios.
     * 
     * @param acp atribuição de cartão provisório
     * 
     * @return true caso adicione
     */
    private boolean addAtribuicao(AtribuirCartaoProvisorio acp) {
        return m_lstAtribuirCartoesProvisorios.add(acp);
    }

    /**
     * Devolve a lista de atribuições de cartões provisórios.
     * 
     * @return a lista de atribuições de cartões provisórios
     */
    public List<AtribuirCartaoProvisorio> getListaAtribuicaoCartoesProvisorios() {
        return this.m_lstAtribuirCartoesProvisorios;
    }

    /**
     * Devolve a atribuição do cartão provisório.
     * 
     * @param sAtribuirCartaoProvisorio a atribuição do cartão provisorio
     * 
     * @return a atribuição do cartão provisorio
     */
    public AtribuirCartaoProvisorio getAtribuirCartaoProvisorio(String sAtribuirCartaoProvisorio) {
        for (AtribuirCartaoProvisorio term : this.m_lstAtribuirCartoesProvisorios) {
            if (term.isIdentifiableAs(sAtribuirCartaoProvisorio)) {
                return term;
            }
        }

        return null;
    }
    
}
