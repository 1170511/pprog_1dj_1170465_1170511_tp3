package pprog_tp3.model;

import java.util.ArrayList;

/**
 * Representa o registo/lista de colaboradores.
 * 
 * @author André e Carlos
 */
public class RegistoDeColaboradores {
    
    /**
     * Lista de colaboradores.
     */
    ArrayList<Colaborador> rcol = new ArrayList<>();

    /**
     * Devolve a lista de colaboradores.
     * 
     * @return lista de colaboradores
     */
    public ArrayList<Colaborador> getListaColaboradores() {
        return rcol;
    }
}
