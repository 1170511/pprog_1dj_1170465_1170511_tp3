package pprog_tp3.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Representa o registo/lista de equipamentos.
 *
 * @author André e Carlos
 */
public class RegistoDeEquipamentos {

    /**
     * A lista de equipamentos.
     */
    private final List<Equipamento> listaEquipamentos;

    /**
     * Constrói uma instância de RegistoDeEquipamentos, inicializando a lista de
     * equipamentos vazia.
     */
    public RegistoDeEquipamentos() {
        this.listaEquipamentos = new ArrayList<Equipamento>();
    }

    /**
     * Cria um novo equipamento
     *
     * @return novo equipamento criado
     */
    public Equipamento novoEquipamento() {
        return new Equipamento();
    }

    /**
     * Devolve true caso o equipamento seja válido e false caso não seja válido.
     *
     * @param e equipamento a validar
     *
     * @return true caso o equipamento seja válido e false caso não seja
     */
    public boolean validaEquipamento(Equipamento e) {
        boolean bRet = false;
//        if (e.valida()) {
//            // Escrever aqui o código de validação
//
//            //
//            bRet = true;
//        }
        return bRet;
    }

    /**
     * Método que faz a validação do equipamento e, caso este seja válido,
     * adiciona-o e retorna true, caso contrário não adiciona e retorna
     * false.
     *
     * @param e equipamento a registar
     *
     * @return true caso seja válido e adicione, false caso contrário
     */
    public boolean registaEquipamento(Equipamento e) {
        if (!this.validaEquipamento(e)) {
            return addEquipamento(e);
        }
        return false;
    }

    /**
     * Adiciona um equipamento à lista de equipamentos.
     * 
     * @param e equipamento a adicionar
     * 
     * @return true caso adicione
     */
    public boolean addEquipamento(Equipamento e) {
        return listaEquipamentos.add(e);
    }

    /**
     * Devolve a lista de equipamentos.
     * 
     * @return a lista de equipamentos
     */
    public List<Equipamento> getListaEquipamentos() {
        return this.listaEquipamentos;
    }

    /**
     * Devolve o equipamento.
     * 
     * @param equipamento o equipamento
     * 
     * @return o equipamento
     */
    public Equipamento getEquipamento(String equipamento) {
        for (Equipamento term : this.listaEquipamentos) {
            if (term.isIdentifiableAs(equipamento)) {
                return term;
            }
        }
        return null;
    }

    /**
     * Devolve o equipamento pelo id.
     * 
     * @param id o id
     * 
     * @return o equipamento
     */
    public Equipamento getEquipamentoById(String id) {
        for (Equipamento e : listaEquipamentos) {
            if (e.getEnderecoLogico().equals(id)) {
                return e;
            }
        }
        return null;
    }
}
