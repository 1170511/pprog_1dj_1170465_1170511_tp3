package pprog_tp3.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Represente o registo/lista dos perfis de autorização.
 * 
 * @author André e Carlos
 */
public class RegistoPerfisDeAutorizacao {

    /**
     * Lista de perfis.
     */
    private final List<PerfilDeAutorizacao> listaPerfis;

    /**
     * Constrói uma instância de RegistoPerfisDeAutorização, inicializando a
     * lista de perfis vazia.
     */
    public RegistoPerfisDeAutorizacao() {
        this.listaPerfis = new ArrayList<PerfilDeAutorizacao>();
    }

    public PerfilDeAutorizacao novoPerfilDeAutorizacao() {
        return new PerfilDeAutorizacao();
    }

    /**
     * Devolve true caso o perfil seja válido e false caso não seja.
     *
     * @param p perfil
     * @return true caso seja válido e false caso contrário.
     */
    public boolean valida(PerfilDeAutorizacao p) {
        boolean a = true;
        return a;
    }

    /**
     * Método que faz a validação do perfil de autorização e, caso este seja
     * válido, adiciona-o e retorna true, caso contrário não adiciona e retorna
     * false.
     *
     * @param p perfil de autorização
     *
     * @return true caso seja válido e adicione, false caso contrário
     */
    public boolean registaPerfilDeAutorizacao(PerfilDeAutorizacao p) {
        if (this.valida(p)) {
            return adicionarPerfil(p);
        }
        return false;
    }

    /**
     * Adiciona o perfil de autorização à lista de perfis.
     *
     * @param perfil perfil de autorização
     *
     * @return true caso adicione
     */
    public boolean adicionarPerfil(PerfilDeAutorizacao perfil) {
        return listaPerfis.add(perfil);
    }

    /**
     * Devolve a lista de perfis de autorização.
     *
     * @return lista de perfis de autorização
     */
    public List<PerfilDeAutorizacao> getListaPerfis() {
        return listaPerfis;
    }
}
