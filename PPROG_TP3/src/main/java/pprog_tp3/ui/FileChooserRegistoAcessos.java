package pprog_tp3.ui;

import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

/**
 *  Representa o FileChooser do registo de acessos
 * 
 * @author André e Carlos
 */
public class FileChooserRegistoAcessos {
       
    /**
     * FileChooser
     */
    private FileChooser fileChooser;

    /**
     * Cria FileChooser para o registo de acessos
     * 
     * @return fileChooser do registo de acessos
     */
    public static FileChooser criarFileChooserRegistoAcessos() {
        FileChooserRegistoAcessos fcRegistoAcessos = new FileChooserRegistoAcessos();

        return fcRegistoAcessos.fileChooser;

    }

    /**
     * Inicializa o FileChooser e define a descrição e extensão do ficheiro.
     */
    private FileChooserRegistoAcessos() {
        fileChooser = new FileChooser();
        
        associarFiltro("Ficheiros de Lista Acessos", "*.ltf");
    }

    /**
     * Método para associar um filtro ao fileChooser.
     * 
     * @param descricao descrição do ficheiro
     * @param extensao extensão do ficheiro
     */
    private void associarFiltro(String descricao, String extensao) {
        ExtensionFilter filtro = new ExtensionFilter(descricao, extensao);

        fileChooser.getExtensionFilters().add(filtro);
    }
    
}
