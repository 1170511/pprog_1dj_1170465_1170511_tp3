package pprog_tp3.ui;

import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

/**
 * Representa o FileChooser do registo de perfis
 * 
 * @author André e Carlos
 */
public class FileChooserRegistoPerfis {
    
    /**
     * FileChooser
     */
    private FileChooser fileChooser;

    /**
     * Cria FileChooser para o registo de perfis
     * 
     * @return fileChooser do registo de perfis
     */
    public static FileChooser criarFileChooserRegistoPerfis() {
        FileChooserRegistoPerfis fcRegistoPerfis = new FileChooserRegistoPerfis();

        return fcRegistoPerfis.fileChooser;

    }

    /**
     * Inicializa o FileChooser e define a descrição e extensão do ficheiro.
     */
    private FileChooserRegistoPerfis() {
        fileChooser = new FileChooser();
        
        associarFiltro("Ficheiros de Lista Perfis", "*.ltf");
    }

    /**
     * Método para associar um filtro ao fileChooser.
     * 
     * @param descricao descrição do ficheiro
     * @param extensao extensão do ficheiro
     */
    private void associarFiltro(String descricao, String extensao) {
        ExtensionFilter filtro = new ExtensionFilter(descricao, extensao);

        fileChooser.getExtensionFilters().add(filtro);
    }

}
