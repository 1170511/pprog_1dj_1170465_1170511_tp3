package pprog_tp3.ui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import pprog_tp3.controllers.JanelaPrincipalController;
import pprog_tp3.model.CartaoIdentificacao;
import pprog_tp3.model.Equipamento;
import pprog_tp3.model.RegistoDeCartoes;
import pprog_tp3.model.RegistoDeEquipamentos;

public class MainApp extends Application {

//    private RegistoDeEquipamentos listaEquipamentos;
    @Override
    public void start(Stage stage) throws Exception {

        try {
            List<Equipamento> lstE= lerFichEquipamentos();
            List<CartaoIdentificacao> lstC = lerFichCartoes();

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/JanelaPrincipalScene.fxml"));
            Parent root = loader.load();

            JanelaPrincipalController jpControler = loader.getController();
            jpControler.associarEquipamentos(lstE);

            Scene scene = new Scene(root);
            scene.getStylesheets().add("/styles/Styles.css");

            stage.setTitle("Aceder à Área Restrita");
            stage.setScene(scene);

            stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    Alert alerta = new Alert(Alert.AlertType.CONFIRMATION);

                    alerta.setTitle("Aplicação");
                    alerta.setHeaderText("Confirmação da ação.");
                    alerta.setContentText("Deseja mesmo encerrar a aplicação?");

                    if (alerta.showAndWait().get() == ButtonType.CANCEL) {
                        event.consume();
                    }
                }
            });

            stage.show();
        } catch (IOException ex) {
            criarAlertaErro(ex).show();
        }
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException a
     */
    public static void main(String[] args) throws FileNotFoundException {
        launch(args);
    }

    /**
     * Método para criar alerta.
     * 
     * @param tipoAlerta o tipo de alerta
     * @param cabecalho o cabeçalho do alerta
     * @param mensagem a mensagem do alerta
     * 
     * @return alerta
     */
    private Alert criarAlertaErro(Exception ex) {
        Alert alerta = new Alert(Alert.AlertType.ERROR);

        alerta.setTitle("Aplicação");
        alerta.setHeaderText("Problemas no arranque da aplicação");
        alerta.setContentText(ex.getMessage());

        return alerta;
    }

    /**
     * Método para ler o ficheiro dos equipamentos
     * 
     * @return lista de equipamentos
     * 
     * @throws FileNotFoundException a
     */
    public List<Equipamento> lerFichEquipamentos() throws FileNotFoundException {
        String[] equipamento = new String[6];
        RegistoDeEquipamentos listaEquipamentos = new RegistoDeEquipamentos();
        File file = new File("Equipamentos.txt");
        Scanner scan = new Scanner(file);
        while (scan.hasNext() == true) {
            equipamento = scan.nextLine().trim().split(";");
            Equipamento e1 = new Equipamento(equipamento[0], equipamento[1], equipamento[2], equipamento[3], equipamento[4], equipamento[5]);
            listaEquipamentos.registaEquipamento(e1);
        }
        return listaEquipamentos.getListaEquipamentos();
    }
    
    /**
     * Método para ler o ficheiro dos cartões
     * 
     * @return lista de cartões
     * 
     * @throws FileNotFoundException a
     */
    public List<CartaoIdentificacao> lerFichCartoes() throws FileNotFoundException {
        String[] cartao = new String[4];
        RegistoDeCartoes listaCartoes = new RegistoDeCartoes();
        File file = new File("Cartoes.txt");
        Scanner scan = new Scanner(file);
        while (scan.hasNext() == true) {
            cartao = scan.nextLine().trim().split(";");
            CartaoIdentificacao c1 = new CartaoIdentificacao(cartao[0], cartao[1], cartao[2], cartao[3]);
            listaCartoes.registaCartao(c1);
        }
        return listaCartoes.getListaCartoes();
    }
}
