/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pprog_tp3.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author André e Carlos
 */
public class PerfilDeAutorizacaoTest {
    
    public PerfilDeAutorizacaoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getId method, of class PerfilDeAutorizacao.
     */
    @Ignore
    public void testGetId() {
        System.out.println("getId");
        PerfilDeAutorizacao instance = new PerfilDeAutorizacao();
        String expResult = "";
        String result = instance.getId();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setId method, of class PerfilDeAutorizacao.
     */
    @Ignore
    public void testSetId() {
        System.out.println("setId");
        String id = "";
        PerfilDeAutorizacao instance = new PerfilDeAutorizacao();
        instance.setId(id);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDescricao method, of class PerfilDeAutorizacao.
     */
    @Ignore
    public void testGetDescricao() {
        System.out.println("getDescricao");
        PerfilDeAutorizacao instance = new PerfilDeAutorizacao();
        String expResult = "";
        String result = instance.getDescricao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDescricao method, of class PerfilDeAutorizacao.
     */
    @Ignore
    public void testSetDescricao() {
        System.out.println("setDescricao");
        String des = "";
        PerfilDeAutorizacao instance = new PerfilDeAutorizacao();
        instance.setDescricao(des);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addEquipamento method, of class PerfilDeAutorizacao.
     */
    @Test
    public void testAddEquipamento() {
        System.out.println("addEquipamento");
        Equipamento e = null;
        PerfilDeAutorizacao instance = new PerfilDeAutorizacao();
        instance.addEquipamento(e);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addPeriodo method, of class PerfilDeAutorizacao.
     */
    @Test
    public void testAddPeriodo() {
        System.out.println("addPeriodo");
        PeriodoDeAutorizacao pa = null;
        PerfilDeAutorizacao instance = new PerfilDeAutorizacao();
        boolean expResult = false;
        boolean result = instance.addPeriodo(pa);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of valida method, of class PerfilDeAutorizacao.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        PerfilDeAutorizacao instance = new PerfilDeAutorizacao();
        boolean expResult = false;
        boolean result = instance.valida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of toString method, of class PerfilDeAutorizacao.
     */
    @Ignore
    public void testToString() {
        System.out.println("toString");
        PerfilDeAutorizacao instance = new PerfilDeAutorizacao();
        String expResult = "";
        String result = instance.toString();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of isIdentifiableAs method, of class PerfilDeAutorizacao.
     */
    @Test
    public void testIsIdentifiableAs() {
        System.out.println("isIdentifiableAs");
        String ID = "";
        PerfilDeAutorizacao instance = new PerfilDeAutorizacao();
        boolean expResult = false;
        boolean result = instance.isIdentifiableAs(ID);
        assertEquals(expResult, result);
    }

    /**
     * Test of isEquipamentoAutorizado method, of class PerfilDeAutorizacao.
     */
    @Test
    public void testIsEquipamentoAutorizado() {
        System.out.println("isEquipamentoAutorizado");
        Equipamento eq = new Equipamento();
        String data = "";
        String hora = "";
        PerfilDeAutorizacao instance = new PerfilDeAutorizacao();
        boolean expResult = false;
        boolean result = instance.isEquipamentoAutorizado(eq, data, hora);
        assertEquals(expResult, result);
    }

    /**
     * Test of converterStringHoraEmIntMinutos method, of class PerfilDeAutorizacao.
     */
    @Test
    public void testConverterStringHoraEmIntMinutos() {
        System.out.println("converterStringHoraEmIntMinutos");
        String hora = "1";
        int expResult = 60;
        int result = PerfilDeAutorizacao.converterStringHoraEmIntMinutos(hora);
        assertEquals(expResult, result);
    }
    
}
